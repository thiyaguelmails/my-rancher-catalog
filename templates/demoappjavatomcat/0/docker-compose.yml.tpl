version: '2'

services:
  java-demo-application:
    restart: always
    image: ${docker_image}
    scale: ${service_scale}
  {{- if eq .Values.use_dns_name_check "true" }}
    labels:
     - "traefik.enable=true"
     - "traefik.domain=${domain_to_register_dns}"
     - "traefik.port=8080"
     - "traefik.alias.fqdn=${dns_name_value}"
     - "traefik.frontend.rule=Host:${dns_name_value}"
     - "java-demo-application"
  {{- end }}