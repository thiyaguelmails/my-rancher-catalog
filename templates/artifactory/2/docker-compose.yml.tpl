# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#        ___         __  _ ____           __                  
#       /   |  _____/ /_(_) __/___ ______/ /_____  _______  __
#      / /| | / ___/ __/ / /_/ __ `/ ___/ __/ __ \/ ___/ / / /
#     / ___ |/ /  / /_/ / __/ /_/ / /__/ /_/ /_/ / /  / /_/ / 
#    /_/  |_/_/   \__/_/_/  \__,_/\___/\__/\____/_/   \__, /  
#                                                    /____/                                                
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

version: '2'

volumes:
  postgresql-data:
    driver: ${volumedriver}
  artifactory-data:
    driver: ${volumedriver}

services:
  postgresql:
    restart: always
    image: docker.bintray.io/postgres:9.5.2
    environment:
       POSTGRES_DB: artifactory
     # The following must match the DB_USER and DB_PASSWORD values passed to Artifactory
       POSTGRES_USER: ${db_username}
       POSTGRES_PASSWORD: ${db_password}
    volumes:
     - postgresql-data:/var/lib/postgresql/data
    ulimits:
      nproc: 65535
      nofile:
        soft: 32000
        hard: 40000
  artifactory:
    restart: always
    container_name: artifactory
    image: docker.bintray.io/jfrog/artifactory-pro:6.1.0
{{- if eq .Values.enable_artifactory_ha "true" }}
    scale: 2
{{- end }}
    depends_on:
     - postgresql
    links:
     - postgresql
    environment:
       DB_TYPE: postgresql
       DB_USER: ${db_username}
       DB_PASSWORD: ${db_password}
    volumes:
     - artifactory-data:/var/opt/jfrog/artifactory
    ulimits:
      nproc: 65535
      nofile:
        soft: 32000
        hard: 40000
  {{- if eq .Values.use_dns_name_check "true" }}
    labels:
     - "traefik.enable=true"
     - "traefik.domain=${domain_to_register_dns}"
     - "traefik.port=8081"
     - "traefik.alias.fqdn=${dns_name_value}"
     - "traefik.frontend.rule=Host:${dns_name_value}"
     - "artifactory"
  {{- end }}