version: "2"

volumes:
  mattermost-db-data:
    driver: ${volumedriver}
  mattermost-app-config:
    driver: ${volumedriver}
  mattermost-app-data:
    driver: ${volumedriver}
  mattermost-app-logs:
    driver: ${volumedriver}
  mattermost-app-plugins:
    driver: ${volumedriver}

services:

  db:
    image: mattermost/mattermost-prod-db:5.2.0
    read_only: true
    restart: unless-stopped
    volumes:
      - mattermost-db-data:/var/lib/postgresql/data
      - /etc/localtime:/etc/localtime:ro
    environment:
      - POSTGRES_USER=${mm_master_username}
      - POSTGRES_PASSWORD=${mm_master_password}
      - POSTGRES_DB=mattermost

  app:
    image: mattermost/mattermost-prod-app:5.2.0
    restart: unless-stopped
    volumes:
      - mattermost-app-config:/mattermost/config:rw
      - mattermost-app-data:/mattermost/data:rw
      - mattermost-app-logs:/mattermost/logs:rw
      - mattermost-app-plugins:/mattermost/plugins:rw
      - /etc/localtime:/etc/localtime:ro
    environment:
      - MM_USERNAME=${mm_master_username}
      - MM_PASSWORD=${mm_master_password}
  {{- if eq .Values.enable_gitlab_oauth_integration "true" }}
      - MM_GITLABSETTINGS_ENABLE=true
      - MM_GITLABSETTINGS_ID=${gitlab_application_id}
      - MM_GITLABSETTINGS_SECRET=${gitlab_application_secret}
      - MM_GITLABSETTINGS_USERAPIENDPOINT=${gitlab_base_url}/api/v3/user
      - MM_GITLABSETTINGS_AuthEndpoint=${gitlab_base_url}/oauth/authorize
      - MM_GITLABSETTINGS_TokenEndpoint=${gitlab_base_url}/oauth/token
  {{- end }}
  web:
    image: mattermost/mattermost-prod-web:5.2.0
    ports:
      - "81:80"
    read_only: true
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime:ro
  {{- if eq .Values.use_dns_name_check "true" }}
    labels:
     - "traefik.enable=true"
     - "traefik.domain=${domain_to_register_dns}"
     - "traefik.port=80"
     - "traefik.alias.fqdn=${dns_name_value}"
     - "traefik.frontend.rule=Host:${dns_name_value}"
     - "mattermost"
  {{- end }}